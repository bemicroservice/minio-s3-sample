from utils.api_utils import generate_object_meta, generate_full_file_name
import datetime


def test_meta_generate():
    test_params = ("testFile.py", "application/json", 455)
    now = str(datetime.datetime.utcnow())
    assert_result = {"last-update": now, "filename": test_params[0], "type": test_params[1],
                     "length": str(test_params[2])}
    generate_result = generate_object_meta(test_params[0], test_params[1], test_params[2])

    assert assert_result.keys() == generate_result.keys()

    for gKey, gVal in generate_result.items():
        if gKey == "last-update":
            continue
        assert generate_result[gKey] == assert_result[gKey]


def test_object_name_validation():
    props = (("", "aaa", 400), ("aaa", "", 400), ("/aaa", "aaa", 400), ("aaa", "/aaa", 400), ("aaa", "aaa.jpg", 200))

    for index, prop in enumerate(props):
        folder, file, assert_code = prop
        code, _ = generate_full_file_name(prop[0], prop[1])
        assert code == assert_code
