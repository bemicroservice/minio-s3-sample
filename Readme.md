# Minio sample
Simple sample of the web service connected to Minio service over S3 HTTP API.

## What is Minio
Open-source object storage with native Kubernetes support. Has compatibility with S3 interface and can be useful for store files and its metadata on self-hosted systems apps.

Supports IAM user policy (pre-defined and custom roles), user groups, S3 events, bucket versioning, content encryption, caching, API configuration, tools for logging, healthchecks etc.

## What can you try in the sample
- Setup Mino instance
- Upload files to folder
- Access files
- Get file metadata

## Install and Try
1. Install Docker
2. Clone the repository.
3. Run the Minio container, can use configuration from `docker-compose.yml`
4. Open `http://localhost:9000` for Minio admin console (GUI)
5. Create new user with `readwrite` access policy.
6. Create test bucket for objects.
7. Run REST application for testing, can use configuration from `docker-compose.yml`, but edit ENV properties for your Minio user and bucket
8. Try REST API with some files