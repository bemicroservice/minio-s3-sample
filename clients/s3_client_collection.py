import json
import minio as minio

from io import BytesIO
from minio.commonconfig import Tags
from minio.datatypes import Object
from urllib3 import HTTPResponse
from urllib3._collections import HTTPHeaderDict
from abc import abstractmethod


class S3_Client:
    @abstractmethod
    def __init__(self, endpoint, access_key, secret_key, bucket):
        """
        Client for the S3 communication using
        :param endpoint: Basic URL of the S3 storage
        :param access_key: IAM Access Key
        :param secret_key: IAM Secret key
        :param bucket: Name of the bucket
        """
        pass

    @abstractmethod
    def save_file_to_storage(self,
                             file: BytesIO,
                             file_name: str,
                             length: int,
                             content_type: str,
                             meta: dict = None,
                             tags: str = "{}"
                             ):
        """
        Saves file to the bucket
        :param file: File content (BytesIO)
        :param file_name: Name or Key of the file
        :param length: length of the centent
        :param content_type: MIME type
        :param meta: Meta tags for the file
        :param tags: Tags as a JSON string
        :return:
        """
        pass

    @abstractmethod
    def load_file_from_storage(self, file_name: str) -> HTTPResponse:
        """
        Load the whole file from the bucket by file path with folder
        :param file_name: relative path including folder
        :return:
        """
        pass

    @abstractmethod
    def get_object_information(self, file_name: str) -> HTTPHeaderDict:
        """
        Load short file information from the bucket by file name / key
        :param file_name: Name of the file / key in bucket including folder path
        :return:
        """
        pass


class MinioClient(S3_Client):
    """
    Implementation of the official Minio S3 client
    """

    def __init__(self, endpoint, access_key, secret_key, bucket):
        self.bucket = bucket
        self.s3 = minio.Minio(self.__format_url(endpoint), access_key, secret_key, secure=False)

        if not self.s3.bucket_exists(self.bucket):
            self.s3.make_bucket(self.bucket)

    @staticmethod
    def __format_url(url_input: str):
        return url_input.replace("http://", "").replace("https://", "").replace("www.", "")

    @staticmethod
    def __format_tags_for_s3(input_tags: str):
        tags_minio = Tags(for_object=True)
        try:
            dict_input: dict = json.loads(input_tags)
            for key, val in dict_input.items():
                tags_minio[key] = val

            return tags_minio

        except:
            print("Tags Error")
            return tags_minio

    def save_file_to_storage(
            self,
            file: BytesIO,
            file_name: str,
            length: int,
            content_type: str,
            meta: dict = None,
            tags: str = ""):
        self.s3.put_object(
            bucket_name=self.bucket,
            data=file,
            object_name=file_name,
            length=length,
            content_type=content_type,
            metadata=meta,
            tags=self.__format_tags_for_s3(tags)
        )

    def load_file_from_storage(self, file_name: str) -> HTTPResponse:
        return self.s3.get_object(bucket_name=self.bucket, object_name=file_name)

    def get_object_information(self, file_name: str) -> HTTPHeaderDict:
        s3_object: Object = self.s3.stat_object(bucket_name=self.bucket, object_name=file_name)
        return s3_object.metadata
