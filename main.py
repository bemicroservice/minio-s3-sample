import io
import json
import uvicorn
from fastapi import FastAPI, File, UploadFile, Request, Response
from clients.s3_client_collection import MinioClient, S3_Client
from utils import environment_reader
from utils.api_utils import generate_object_meta, generate_full_file_name

app = FastAPI()

environs = environment_reader.read_environments()

minio_client_s3: S3_Client = MinioClient(
    endpoint=environs.s3_endpoint,
    access_key=environs.s3_access,
    secret_key=environs.s3_secret,
    bucket=environs.s3_bucket
)


@app.get("/")
def index():
    """
    I am alive endpoint
    :return:
    """
    return {"Server": "Works"}


@app.get("/get-file/{name:path}")
def get_file(name: str):
    """
    Returns file by URL part
    :param name: Sub-part of the URL to the file
    :return: File content with headers
    """
    s3_object = minio_client_s3.load_file_from_storage(file_name=name)
    return Response(headers=s3_object.headers, content=s3_object.data)


@app.get("/get-file-info/{name:path}")
def get_file_info(name: str):
    """
    Returns file information with no contenn, just metadata
    :param name: Sub-part of the URL to the file
    :return: File metadata
    """
    s3_metadata = minio_client_s3.get_object_information(file_name=name)
    return Response(content=json.dumps(dict(s3_metadata)))


@app.post("/save-file")
async def save_file(request: Request, file: UploadFile = File(...)):
    """
    Saves / overrides file saved on object storage
    :param request: Request with the file
    :param file: File content
    :return: Metadata about saved file
    """
    folder = request.headers.get("folder")
    tags = request.headers.get("tags")

    code, filename_result = generate_full_file_name(folder, file.filename)
    if code != 200:
        return Response(status_code=code, content=filename_result)

    content = await file.read()
    file_length = len(content)
    file_record_meta = generate_object_meta(file.filename, file.content_type, file_length)
    minio_client_s3.save_file_to_storage(
        file=io.BytesIO(content),
        file_name=filename_result,
        length=file_length,
        content_type=file.content_type,
        meta=file_record_meta,
        tags=tags
    )
    return file_record_meta


if __name__ == '__main__':
    uvicorn.run(app, host=environs.host, port=environs.port)
